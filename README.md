# Advent of code 2021

## How to initialize a new day
```shell
cargo new src/day-4
```

## How to run specific day
```shell
cargo run -p day-2
```

## How to watch automatically 
```shell
cargo-watch -x "run -p day-<X>"
```

## How to build all
```shell
cargo build --workspace
```

## How to test all the workspace
```shell
cargo test --workspace
```