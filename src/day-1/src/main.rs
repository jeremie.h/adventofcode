use std::error::Error;
use util::common::*;

fn main() -> Result<(), Box<dyn Error>> {
    display_msg_1();
    println!("compteur {}", part1());

    display_msg_2();
    println!("compteur {}", part2());

    Ok(())
}

fn part1() -> i32 {
    let mut monmax = 0;
    let mut compteur = 0;
    for line in load_string() {
        let monentier = line.parse::<i32>().unwrap();
        if monentier > monmax {
            compteur = compteur + 1;
        }
        monmax = monentier;
    }
    compteur - 1
}

fn part2() -> i32 {
    let (mut a, mut b, mut c) = (0, 0, 0);
    let (mut totaln, mut totalnmoins1);
    let mut compteur = 0;
    for line in load_string() {
        let monentier = line.parse::<i32>().unwrap();
        if a == 0 {
            a = monentier;
            continue;
        }
        if b == 0 {
            b = monentier;
            continue;
        }
        if c == 0 {
            c = monentier;
            continue;
        }

        totalnmoins1 = a + b + c;

        a = b;
        b = c;
        c = monentier;
        totaln = a + b + c;

        if totaln > totalnmoins1 {
            compteur = compteur + 1;
        }
    }
    println!("{}", compteur);
    compteur
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_all() {
        assert_eq!(part1(), 1475);
        assert_eq!(part2(), 1516);
    }
}
