use std::error::Error;
use util::common::*;

fn main() -> Result<(), Box<dyn Error>> {
    display_msg_1();
    println!("forward x depth = {}", part1());

    display_msg_2();
    println!("forward x depth = {}", part2());
    Ok(())
}

fn part1() -> i32 {
    let lines = load_string();
    let (mut forward, mut depth) = (0, 0);
    for line in lines {
        let length = line.len();
        //let mut parts = line.split_whitespace().map(|s| s.parse::<i32>());
        let firstchar = line.get(..1);
        match firstchar {
            Some("f") => {
                forward += line
                    .get(length - 1..length)
                    .unwrap()
                    .parse::<i32>()
                    .unwrap()
            }
            Some("u") => {
                depth -= line
                    .get(length - 1..length)
                    .unwrap()
                    .parse::<i32>()
                    .unwrap()
            }
            Some("d") => {
                depth += line
                    .get(length - 1..length)
                    .unwrap()
                    .parse::<i32>()
                    .unwrap()
            }
            None => println!("None"),
            _ => println!("unknown"),
        }
    }
    forward * depth
}

fn part2() -> i32 {
    let lines = load_string();
    let (mut forward, mut depth, mut aim) = (0, 0, 0);
    for line in lines {
        let length = line.len();
        //let mut parts = line.split_whitespace().map(|s| s.parse::<i32>());
        let firstchar = line.get(..1);
        match firstchar {
            Some("f") => {
                let x = line
                    .get(length - 1..length)
                    .unwrap()
                    .parse::<i32>()
                    .unwrap();
                forward += x;
                depth += aim * x;
            }
            Some("u") => {
                let x = line
                    .get(length - 1..length)
                    .unwrap()
                    .parse::<i32>()
                    .unwrap();
                aim -= x;
            }
            Some("d") => {
                let x = line
                    .get(length - 1..length)
                    .unwrap()
                    .parse::<i32>()
                    .unwrap();
                aim += x;
            }
            None => println!("None"),
            _ => println!("unknown"),
        }
    }
    forward * depth
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_all() {
        assert_eq!(part1(), 1868935);
        assert_eq!(part2(), 1965970888);
    }
}
