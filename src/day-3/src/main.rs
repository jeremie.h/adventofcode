use std::error::Error;
use util::common::*;

/**
 * Main function
 * run part 1 & part 2
 */
fn main() -> Result<(), Box<dyn Error>> {
    display_msg_1();
    println!("gamma*epsilon = {}", part1());

    display_msg_2();
    println!("co2*oxygen = {}", part2());
    Ok(())
}

/**
 * Part 1
 */
fn part1() -> i32 {
    let mut gamma = 0;
    let epsilon;
    let tab: Vec<u16> = load_vec16();

    let mut compteur_bits: [i32; 12] = [0; 12];
    for one_elt in tab {
        for i in (0..12).rev() {
            if (one_elt & 0x01 << i) == 0 {
                compteur_bits[11 - i] -= 1;
            } else {
                compteur_bits[11 - i] += 1;
            }
        }
    }
    println!("compteur_bits {:?}", compteur_bits);
    let mut i: i32 = 11;
    for e in compteur_bits {
        let bit = if e > 0 { 1 } else { 0 };
        gamma += bit << i;
        i -= 1;
    }
    println!("gamma = {}", gamma);
    epsilon = !gamma & 0b111111111111;
    gamma * epsilon
}
/**
 * Part 2
 */
fn part2() -> i32 {
    let (oxygen, co2);
    let tab: Vec<u16> = load_vec16();

    let compteur_bits: [i32; 12] = [0; 12];
    let mut copie_tab: Vec<u16> = tab.clone();
    //search oxygen
    let closure_oxygen = |entier: u16, indice: usize, valeur: i32| {
        if valeur >= 0 {
            entier & 0x01 << indice == 0x01 << indice
        } else {
            !entier & 0x01 << indice == 0x01 << indice
        }
    };
    oxygen = loop_inputs(&mut copie_tab, compteur_bits, closure_oxygen);
    println!("oxygen = {}", oxygen);

    //search co2
    copie_tab = tab.clone();
    //compteur_bits = [0; 12];
    let closure_co2 = |entier: u16, indice: usize, valeur: i32| {
        if valeur < 0 {
            entier & 0x01 << indice == 0x01 << indice
        } else {
            !entier & 0x01 << indice == 0x01 << indice
        }
    };
    co2 = loop_inputs(&mut copie_tab, compteur_bits, closure_co2);
    println!("co2 = {}", co2);
    co2 as i32 * oxygen as i32
}

fn loop_inputs(
    tableau: &mut Vec<u16>,
    mut compteur_bits: [i32; 12],
    filtre: impl Fn(u16, usize, i32) -> bool,
) -> u16 {
    for i in (0..12).rev() {
        for un_entier in &*tableau {
            if (un_entier & 0x01 << i) == 0 {
                compteur_bits[11 - i] = compteur_bits[11 - i] - 1;
            } else {
                compteur_bits[11 - i] = compteur_bits[11 - i] + 1;
            }
        }
        //println!("compteur_bits {:?}", compteur_bits);
        *tableau = tableau
            .iter()
            .cloned()
            .filter(|e| filtre(*e, i, compteur_bits[11 - i]))
            .collect::<Vec<u16>>();

        // if tableau.len() < 15 {
        //     println!("nouveau tab {:?}", tableau);
        //     for e in &*tableau {
        //         println!("{:b}",e);
        //     }
        // }

        if tableau.len() < 2 {
            break;
        }
    }
    tableau[0]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_all() {
        assert_eq!(part1(), 2003336);
        assert_eq!(part2(), 1877139);
    }
}
