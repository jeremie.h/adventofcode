use std::error::Error;
use util::{bingo::*, common::*};

fn main() -> Result<(), Box<dyn Error>> {
    display_msg_1();
    part1()?;

    display_msg_2();
    part2()?;
    Ok(())
}

/**
 * Part 1
 */
fn part1() -> Result<i32, Box<dyn Error>> {
    let mut boards = load_board_bingo();
    let score;
    for drawnumber in load_drawn_number() {
        for board in boards.iter_mut() {
            if board.draw(drawnumber) {
                println!(
                    "au numéro {} nous avons un board gagnant  : {:?}",
                    drawnumber, board
                );
                score = drawnumber * board.score_without_latest();
                println!("score final : {:?}", score);
                return Ok(score);
            }
        }
    }
    Ok(0)
}

/**
 * Part 2
 */
fn part2() -> Result<i32, Box<dyn Error>> {
    let mut boards = load_board_bingo();
    let score;
    for drawnumber in load_drawn_number() {
        for board in boards.iter_mut() {
            if board.draw(drawnumber) && board.is_last {
                println!(
                    "au numéro {} nous avons un board gagnant  : {:?}",
                    drawnumber, board
                );
                score = drawnumber * board.score_without_latest();
                println!("score final : {:?}", score);
                return Ok(score);
            }
        }

        let mut lesderniers: Vec<&mut Board> = boards
            .iter_mut()
            .filter(|b| !b.is_winning())
            .collect::<Vec<&mut Board>>();

        if lesderniers.len() == 1 {
            lesderniers[0].set_last()
        }
    }
    Ok(0)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_all() {
        // with exemple inputs
        // assert_eq!(part1().unwrap(), 4512);
        // assert_eq!(part2().unwrap(), 1924);
        // with exercices inputs
        assert_eq!(part1().unwrap(), 33348);
        assert_eq!(part2().unwrap(), 8112);
    }
}
