use std::{
    cmp::{max, min},
    error::Error,
};
use util::common::*;

fn main() -> Result<(), Box<dyn Error>> {
    display_msg_1();
    let result1 = part1()?;
    println!("result part 1 : {}", result1);

    display_msg_2();
    let result2 = part2()?;
    println!("result part 2 : {}", result2);
    Ok(())
}

/**
 * load deplacements from inputs
 */
#[rustfmt::skip]
fn load_deplacements() -> Vec<Vec<(i32, i32)>> {
    load_string()
        .iter()
        .map(|ligne| {
            ligne
                .split(" -> ")
                .map(|pos| {
                    pos.split(",").fold((-1, -1), |(x, _y), coord| {
                        let coord_entier = coord.parse().unwrap();
                        if x == -1 { (coord_entier, -1) }
                        else { (x, coord_entier) }
                    })
                })
                .fold(vec![(0, 0); 0], |mut depl, (x, y)| {
                    depl.push((x, y));
                    depl
                })
        })
        .collect::<Vec<Vec<(i32, i32)>>>()
}

/**
 * initialize the ocean from the input.txt
 */
fn init_ocean(deplacements: &Vec<Vec<(i32, i32)>>) -> Vec<Vec<i32>> {
    let taille_ocean = match deplacements[0][0].0 {
        x if x > 10 => 1000,
        _ => 10,
    };
    vec![vec![0 as i32; taille_ocean]; taille_ocean]
}

/**
 * compute the score of the resulting ocean
 */
#[rustfmt::skip]
fn score_ocean(ocean: &[Vec<i32>], display: bool) -> i32 {
    let mut compteur = 0;
    ocean.iter().for_each(|ligne| {
        ligne.iter().for_each(|col| {
            if display {
                if *col == 0 { print!(".") }
                else { print!("{}", col) }
            }
            if *col > 1 { compteur += 1 }
        });
        if display {
            println!()
        }
    });
    compteur
}

/**
 * trace the line between two points (x1,y1) and (x2,y2)
 */
#[rustfmt::skip]
fn trace_line(depl: &[(i32, i32)], ocean: &mut [Vec<i32>]) {
    let x1 = depl[0].0;
    let y1 = depl[0].1;
    let x2 = depl[1].0;
    let y2 = depl[1].1;

    //gestion horizontales et verticales
    if x1 == x2 {
        for i in min(y1, y2)..=max(y1, y2) {
            ocean[i as usize][x1 as usize] = ocean[i as usize][x1 as usize] + 1;
        }
    } else if y1 == y2 {
        for j in min(x1, x2)..=max(x1, x2) {
            ocean[y1 as usize][j as usize] = ocean[y1 as usize][j as usize] + 1;
        }
    }
    //gestion diagonales
    else {
        let mut j = 0;
        let mut inc: i32 = 0;
        for i in min(y1, y2)..=max(y1, y2) {
            if i == y1 {
                j = x1;
                if x1 < x2 {inc = 1} else {inc = -1}
            }
            if i == y2 {
                j = x2;
                if x1 > x2 {inc = 1} else {inc = -1}
            }
            ocean[i as usize][j as usize] = ocean[i as usize][j as usize] + 1;
            j += inc;
        }
    }
}

/**
 * Part 1
 */
fn part1() -> Result<i32, Box<dyn Error>> {
    let deplacements = load_deplacements();

    //init ocean
    let mut ocean = init_ocean(&deplacements);

    //process
    deplacements
        .iter()
        //filtre uniquement sur les horizontales et verticales
        .filter(|depl| depl[0].0 == depl[1].0 || depl[0].1 == depl[1].1)
        .for_each(|depl| {
            trace_line(depl, &mut ocean);
        });

    //compute & display ocean
    let compteur = score_ocean(&ocean, ocean.len() == 10);
    Ok(compteur)
}

/**
 * Part 2
 */
fn part2() -> Result<i32, Box<dyn Error>> {
    let deplacements = load_deplacements();

    //init ocean
    let mut ocean = init_ocean(&deplacements);

    //process
    deplacements.iter().for_each(|depl| {
        trace_line(depl, &mut ocean);
    });

    //compute & display ocean
    let compteur = score_ocean(&ocean, ocean.len() == 10);
    Ok(compteur)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_all() {
        // with exemple inputs
        // assert_eq!(part1().unwrap(), 5);
        // assert_eq!(part2().unwrap(), 12);
        // with exercices inputs
        assert_eq!(part1().unwrap(), 8111);
        assert_eq!(part2().unwrap(), 22088);
    }
}
