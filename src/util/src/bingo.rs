use crate::common::*;
use array2d::Array2D;

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct Cell {
    value: i32,
    drawn: bool,
}

impl Cell {
    pub fn new() -> Self {
        Self {
            value: 0,
            drawn: false,
        }
    }
    pub fn from_val(val: i32) -> Self {
        Self {
            value: val,
            drawn: false,
        }
    }
    fn drawn(&mut self) {
        self.drawn = true;
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Board {
    cells: Array2D<Cell>,
    pub is_last: bool,
}

impl Board {
    #[allow(unused)]
    pub fn new() -> Self {
        Self {
            cells: Array2D::filled_with(Cell::new(), 5, 5),
            is_last: false,
        }
    }

    pub fn set_last(&mut self) {
        self.is_last = true;
    }
    pub(crate) fn fill(&mut self, five_lines: Vec<i32>) {
        let cellules = five_lines
            .iter()
            .map(|f| Cell::from_val(*f))
            .collect::<Vec<Cell>>();
        self.cells = Array2D::from_row_major(&cellules, 5, 5)
    }

    pub fn draw(&mut self, numero: i32) -> bool {
        for i in 0..5 {
            for j in 0..5 {
                if let Some(cellule) = self.cells.get_mut(i, j) {
                    if cellule.value == numero {
                        cellule.drawn()
                    }
                }
            }
        }
        self.is_winning()
    }

    pub fn is_winning(&self) -> bool {
        let check_by_rows = self
            .cells
            .rows_iter()
            .any(|mut row| row.all(|cellule| cellule.drawn));
        let check_by_columns = self
            .cells
            .columns_iter()
            .any(|mut column| column.all(|cellule| cellule.drawn));

        check_by_rows | check_by_columns
    }

    pub fn score_without_latest(&self) -> i32 {
        let score = self
            .cells
            .elements_row_major_iter()
            .filter(|cell| !cell.drawn)
            .map(|cell| cell.value)
            .sum();
        score
    }
}

/**
 * load drawn number for bingo
 */
pub fn load_drawn_number() -> Vec<i32> {
    let strings = load_string();

    let drawn_numbers = strings
        .get(0)
        .unwrap()
        .split(",")
        //.inspect(|f| println!("{:?}",f))
        .map(|line| line.parse::<i32>().unwrap())
        .collect::<Vec<i32>>();
    println!("drawns numbers : {:?}", drawn_numbers);
    drawn_numbers
}

/**
 *  load board for bingo
 */
pub fn load_board_bingo() -> Vec<Board> {
    let strings = load_string();

    let (result, _, _) = strings
        .split_at(2)
        .1
        .iter()
        .filter(|ligne| !ligne.is_empty())
        .fold(
            (Vec::new(), String::new(), 0),
            |(mut result, mut chaine, mut compteur), s_param| {
                if compteur > 4 {
                    let mut current_board = Board::new();
                    current_board.fill(
                        chaine
                            .split_whitespace()
                            .map(|i| i.parse().unwrap())
                            .collect(),
                    );
                    result.push(current_board);
                    chaine = String::new();
                    compteur = 0;
                }

                (result, chaine + " " + s_param, compteur + 1)
            },
        );

    result
}
