extern crate colored;

use colored::*;
use std::env;
use std::fs::File;
use std::io::{prelude::*, BufReader};

fn get_day() -> i32 {
    match env::current_exe() {
        Ok(exe_path) => {
            let directory = exe_path.file_name().unwrap().to_str().unwrap();
            if let Some(tiret) = String::from(directory).find("-") {
                directory[tiret + 1..].parse::<i32>().unwrap()
            } else {
                0
            }
        }
        Err(e) => panic!("failed to get current exe path: {}", e),
    }
}

pub(crate) fn get_reader() -> BufReader<File> {
    match env::current_exe() {
        Ok(exe_path) => {
            let id = exe_path.file_name().unwrap().to_str().unwrap();
            let root_loc = "src/".to_owned() + id + "/src/input.txt";
            let file = File::open(root_loc);
            match file {
                Ok(unw_file) => BufReader::new(unw_file),
                Err(_) => {
                    //in case of tests, just open src/input.txt
                    match File::open("src/input.txt") {
                        Ok(unw_file) => BufReader::new(unw_file),
                        Err(error) => panic!(
                            "Problem opening the file: {} src/{}/src/input.txt",
                            error,
                            env!("CARGO_PKG_NAME")
                        ),
                    }
                }
            }
        }
        Err(e) => panic!("failed to get current exe path: {}", e),
    }
}

/**
 *  load inputs file in Vec<u16>
 */
pub fn load_vec16() -> Vec<u16> {
    let numbers: Vec<u16> = get_reader()
        .lines()
        //.inspect(|f| println!("{:?}",f))
        .map(|line| u16::from_str_radix(&line.unwrap(), 2).unwrap())
        .collect();
    numbers
}

/**
 *  load inputs file in Vec<u16>
 */
pub fn load_string() -> Vec<String> {
    let strings: Vec<String> = get_reader()
        .lines()
        .map(|l| l.expect("Could not parse string from file"))
        .collect();
    strings
}

pub fn display_msg_1() {
    let day = get_day();
    println!("{}", format!("==== Day {} - Part {} ====", day, 1).cyan());
}
pub fn display_msg_2() {
    let day = get_day();
    println!("{}", format!("==== Day {} - Part {} ====", day, 2).cyan());
}
